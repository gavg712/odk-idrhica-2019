# Charlas IDRHiCA 2019 

Fecha: _19-Dic-2019_

![](img/ODK_R.png)

# Open Data Kit + R: Herramientas para recolección de datos

La ponencia está orientada a investigadores, estudiantes universitarios o personas
que requieren de levantamiento de información de campo para proyectos pequeños. 
Se presenta sobre una estrategia simple para recolección de información en campo
con ODK Collect en Android y sistematización con Tidyverse en R. 
Esta estrategia de recolección asume que la persona que requiere datos de campo, 
no tiene acceso a servidores (hardware o software) que le permitan usar 
plataformas disponibles actualmente en Internet
